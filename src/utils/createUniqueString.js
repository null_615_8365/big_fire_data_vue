/*
 * Copyright (c) 2019.  武汉中科图灵科技有限公司
 * Date :  2019/1/5
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.6.2
 */
export default function createUniqueString() {
  const timestamp = +new Date() + ''
  const randomNum = parseInt((1 + Math.random()) * 65536) + ''
  return (+(randomNum + timestamp)).toString(32)
}
