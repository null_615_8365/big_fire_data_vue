/*
 * Copyright (c) 2019.  武汉中科图灵科技有限公司
 * Date :  2019/1/5
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.6.2
 */

export function getAllChildrenIds(ids, data) {
  if (data.children !== undefined) {
    for (let i = 0; i < data.children.length; i++) {
      ids.push(data.children[i].id);
      const child = data.children[i]
      getAllChildrenIds(ids, child)
    }
  } else {
    return;
  }
}
