/*
 * Copyright (c) 2019.  武汉中科图灵科技有限公司
 * Date :  2019/1/5
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.6.2
 */

import Cookies from 'js-cookie'
import { setItem, getItem, removeItem } from '@/utils/nocookie'
const TokenKey = 'Authorization'

export function getToken() {
  return getItem(TokenKey)!==undefined?getItem(TokenKey):''
}

export function setToken(token) {
  return setItem(TokenKey, token)
}

export function removeToken() {
  return removeItem(TokenKey)
}

