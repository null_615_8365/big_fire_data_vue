/*
 * Copyright (c) 2019.  武汉中科图灵科技有限公司
 * Date :  2019/1/5
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.6.2
 */

import request from '@/utils/request'

export function fetchTree() {
    return request({
        url: '/api/device/deviceBuilding/select',
        method: 'get',
    })
}

export function firstBuild() {
    return request({
        url: '/api/device/deviceBuilding/selectFirst',
        method: 'get',
    })
}

export function page(query) {
    return request({
        url: '/api/device/deviceSensor/pageList',
    method: 'get',
    params: query
    })
}
export function pageCode(query) {
    return request({
        url: '/api/device/deviceBuilding/pageCode',
        method: 'get',
        params: query
    })
}

export function getBuidldInfo(id) {
    return request({
        url: '/api/device/deviceBuilding/get?id='+id,
        method: 'get'
    })
}

export function delBuild(id) {
    return request({
        url: '/api/device/deviceBuilding/delete?id='+id,
        method: 'get'
    })
}

export function addBuildObj(obj) {
    return request({
        url: '/api/device/deviceBuilding/add',
        method: 'post',
        data: obj
    })
}

export function putBuildObj(obj) {
    return request({
        url: '/api/device/deviceBuilding/update',
        method: 'post',
        data: obj
    })
}

export function getBuidldImg(query) {
    return request({
        url: '/api/device/deviceFloorLayout/selectImage',
        method: 'get',
        params: query
    })
}

export function getCurFloor(query){
    return request({
        url: '/api/device/deviceBuilding/getFloorList',
        method: 'get',
        params: query
    })
}

export function upLoadImage(obj) {
    return request({
        url: '/api/device/deviceFloorLayout/uploadImage',
        method: 'post',
        data: obj
    })
}

export function getNetworkAll(){
    return request({
        url: '/api/device/deviceNetworkingUnit/getAll',
        method: 'get'
    })
}

export function delFloorImg(obj) {
  return request({
    url: '/api/device/deviceFloorLayout/deleteImage',
    method: 'delete',
    data:obj
  })
}

export function delBuildQuery(query) {
  return request({
    url: '/api/device/deviceBuilding/deleteQuery',
    method: 'get',
    params:query
  })
}
