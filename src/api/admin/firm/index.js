/*
 * Copyright (c) 2019.  武汉中科图灵科技有限公司
 * Date :  2019/1/5
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.6.2
 */

import request from '@/utils/request'

export function page(query) {
    return request({
        url: '/api/device/deviceSensorManufacturer/pageList',
        method: 'get',
        params: query
    })
}

export function devicePage(query) {
    return request({
        url: '/api/device/deviceCollectingManufacturer/pageList',
        method: 'get',
        params: query
    })
}

export function devicePoint() {
    return request({
        url: '/api/device/deviceMeasuringPoint/all',
        method: 'get',
    })
}

export function addObj(obj) {
    return request({
        url: '/api/device/deviceSensorManufacturer/add',
        method: 'post',
        data: obj
    })
}

export function deviceAddObj(obj) {
    return request({
        url: '/api/device/deviceCollectingManufacturer/add',
        method: 'post',
        data: obj
    })
}

export function deleteQuery(id) {
    return request({
        url: '/api/device/deviceSensorManufacturer/deleteQuery?id=' + id,
        method: 'get',
    })
}

export function deviceDeleteQuery(id) {
    return request({
        url: '/api/device/deviceCollectingManufacturer/deleteQuery?id=' + id,
        method: 'get',
    })
}

export function getObj(id) {
    return request({
        url: '/api/device/deviceSensorManufacturer/get?id='+id,
        method: 'get'
    })
}

export function deviceGetObj(id) {
    return request({
        url: '/api/device/deviceCollectingManufacturer/get?id=' + id,
        method: 'get'
    })
}

export function delObj(id) {
    return request({
        url: '/api/device/deviceSensorManufacturer/delete?id=' + id,
        method: 'get'
    })
}

export function deviceDelObj(id) {
    return request({
        url: '/api/device/deviceCollectingManufacturer/delete?id=' + id,
        method: 'get'
    })
}

export function putObj(obj) {
    return request({
        url: '/api/device/deviceSensorManufacturer/update',
        method: 'post',
        data: obj
    })
}

export function devicePutObj(obj) {
    return request({
        url: '/api/device/deviceCollectingManufacturer/update',
        method: 'post',
        data: obj
    })
}
