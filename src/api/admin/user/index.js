/*
 * Copyright (c) 2019.  武汉中科图灵科技有限公司
 * Date :  2019/1/5
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.6.2
 */

import request from '@/utils/request'

export function page(query) {
  return request({
    url: '/api/admin/user/list',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/api/admin/user/add',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/api/admin/user/' + id,
    method: 'get'
  })
}

export function getUserRole(query){
  return request({
    url: '/api/admin/group/listByUser',
    method: 'get',
    params: query
  })
}

export function delObj(query) {
  return request({
    url: '/api/admin/user/delete',
    method: 'get',
    params: query
  })
}

export function updateObj(obj) {
  return request({
    url: '/api/admin/user/update',
    method: 'post',
    data: obj
  })
}


export function changePassword(data) {
  return request({
    url: '/api/admin/user/changePassword',
    method: 'post',
    params: data
  })
}

export function getGroup(){
  return request({
    url:'/api/admin/group/all',
    method:'get'
  })
}
