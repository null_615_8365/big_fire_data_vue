/*
 * Copyright (c) 2019.  武汉中科图灵科技有限公司
 * Date :  2019/1/5
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.6.2
 */

import request from '@/utils/request';

export function getBuildAll(query) {
    return request({
        url: '/api/device/deviceNetworkingUnit/getBuildingList',
        method: 'get',
        params: query
    })
}

export function getUnitAll(query) {
    return request({
        url: '/api/device/deviceNetworkingUnit/getAll?oName='+query,
        method: 'get'
    })
}

export function deleteSearch(query) {
    return request({
        url: '/api/device/deviceNetworkingUnit/deleteQuery?id='+query,
        method: 'get'
    })
}

export function deleteUnit(query) {
    return request({
        url: '/api/device/deviceNetworkingUnit/delete?id='+query,
        method: 'get'
    })
}

export function updateUnit(obj) {
  return request({
    url: '/api/device/deviceNetworkingUnit/update',
    method: 'post',
    data: obj
  })
}

export function getOneUnitInfo(id) {
    return request({
        url: '/api/device/deviceNetworkingUnit/get?id='+id,
        method: 'get'
    })
}

export function addUnitObj(obj) {
  return request({
    url: '/api/device/deviceNetworkingUnit/add',
    method: 'post',
    data: obj
  })
}
