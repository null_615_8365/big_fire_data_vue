/*
 * Copyright (c) 2019.  武汉中科图灵科技有限公司
 * Date :  2019/1/5
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.6.2
 */

import request from '@/utils/request'

export function getAllCount() {
  return request({
    url: '/api/device/deviceSensor/getAllCount',
    method: 'get',
  })
}
//pei图表
export function getPiechart() {
  return request({
    url: '/api/datahandler/deviceFacilitiesAbnormal/getRatio',
    method: 'get',
  })
}

//报警图表
export function getalram() {
  return request({
    url: '/api/datahandler/deviceFacilitiesAbnormal/getCallCount',
    method: 'get',
  })
}
//故障图表
export function getabnormal() {
  return request({
    url: '/api/datahandler/deviceFacilitiesAbnormal/getFaultCount',
    method: 'get',
  })
}
//地图上的数据集合
export function getMapData() {
  return request({
    url: '/api/datahandler/deviceFacilitiesAbnormal/getCount',
    method: 'get',
  })
}

// 概览新增接口
export function getAllBuildingStatus() {
  return request({
    url: '/api/device/deviceBuilding/getAllBuildingStatus',
    method: 'get'
  })
}
