/*
 * Copyright (c) 2019.  武汉中科图灵科技有限公司
 * Date :  2019/1/5
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.6.2
 */

import request from '@/utils/request'

export function getRatioByDate(query) {
  return request({
    url: '/api/datahandler/deviceFacilitiesAbnormal/getRatioByDate',
    method: 'get',
    params: query
  })
}
export function selectCountByType(query) {
  return request({
    url: '/api/datahandler/deviceFacilitiesAbnormal/selectCountByType',
    method: 'get',
    params: query
  })
}
export function selectFaultCountByMonth(query) {
  return request({
    url: '/api/datahandler/deviceFacilitiesAbnormal/selectFaultCountByMonth',
    method: 'get',
    params: query
  })
}
