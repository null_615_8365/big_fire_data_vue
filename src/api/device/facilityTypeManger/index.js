/*
 * Copyright (c) 2019.  武汉中科图灵科技有限公司
 * Date :  2019/1/5
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.6.2
 */

import request from '@/utils/request'

export function page(query) {
  //table
  return request({
    url: '/api/device/deviceFacilitiesType/pageList',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/api/device/deviceFacilitiesType/add',
    method: 'post',
    data: obj
  })
}

export function putObj(obj) {
  return request({
    url: '/api/device/deviceFacilitiesType/update',
    method: 'post',
    data: obj
  })
}

export function getObj(query) {
  return request({
    url: '/api/device/deviceFacilitiesType/get',
    method: 'get',
    params: query
  })
}

export function deleteQuery(query) {
  return request({
    url: '/api/device/deviceFacilitiesType/deleteQuery',
    method: 'get',
    params: query
  })
}

export function delObj(query) {
  return request({
    url: '/api/device/deviceFacilitiesType/delete',
    method: 'get',
    params: query
  })
}
