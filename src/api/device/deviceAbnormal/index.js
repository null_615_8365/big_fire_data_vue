import request from '@/utils/request'
// 视频异常记录数据接口
export function page(query) {
    return request({
      url: '/api/device/videoAbnormal/queryList',
      method: 'get',
      params: query
    })
}
// 报警类型下拉框
export function selectAlrmType() {
  return request({
    url: '/api/device/videoAbnormal/alarmTypes',
    method: 'get'
  })
}
// 高级查询下拉框
export function deviceSerials() {
  return request({
    url: '/api/device/videoAbnormal/deviceSerials',
    method: 'get'
  })
}
