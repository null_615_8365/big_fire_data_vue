/*
 * Copyright (c) 2019.  武汉中科图灵科技有限公司
 * Date :  2019/1/5
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.6.2
 */

import request from '@/utils/request'

export function page(query) {
return request({
url: '/api/device/deviceDataDictionary/page',
method: 'get',
params: query
})
}

export function addObj(obj) {
return request({
url: '/api/device/deviceDataDictionary',
method: 'post',
data: obj
})
}

export function getObj(id) {
return request({
url: '/api/device/deviceDataDictionary/' + id,
method: 'get'
})
}

export function delObj(id) {
return request({
url: '/api/device/deviceDataDictionary/' + id,
method: 'delete'
})
}

export function putObj(id, obj) {
return request({
url: '/api/device/deviceDataDictionary/' + id,
method: 'put',
data: obj
})
}
