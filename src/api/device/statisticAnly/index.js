/*
 * Copyright (c) 2019.  武汉中科图灵科技有限公司
 * Date :  2019/1/5
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.6.2
 */

import request from '@/utils/request'

export function selectCountByBuild(query) {
    return request({
    url: '/api/datahandler/deviceAbnormal/selectCountByBuildId',
    method: 'get',
    params: query
    })
}

export function CountByType(query) {
    return request({
    url: '/api/datahandler/deviceAbnormal/selectCountByType',
    method: 'get',
    params: query
    })
}

export function CountByMonth(query) {
    return request({
    url: '/api/datahandler/deviceAbnormal/selectCountByDate',
    method: 'get',
    params: query
    })
}
//隐患处理情况
export function getDefect(query) {
  return request({
    url: '/api/datahandler/deviceAbnormal/getRatioByDate',
    method: 'get',
    params: query
  })
}
