/*
 * Copyright (c) 2019.  武汉中科图灵科技有限公司
 * Date :  2019/1/5
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.6.2
 */

import request from "@/utils/request";

export function selectCount(query) {
  return request({
    url: "/api/datahandler/deviceAbnormal/selectCount",
    method: "get",
    params: query
  });
}

export function selectCountChart(query) {
  return request({
    url: "/api/datahandler/deviceAbnormal/selectCountNearlyMonth",
    method: "get",
    params: query
  });
}


export function selectAlrmType(obj) {
  return request({
    url: "/api/datahandler/deviceAbnormal/selectAlrmType",
    method: "get",
    params: obj
  });
}

export function pageList(query) {
  return request({
    url: "/api/datahandler/deviceAbnormal/pageList",
    method: "get",
    params: query
  });
}

export function selectCountByBuild(query) {
  return request({
    url: "/api/device/deviceBuilding/getSelected",
    method: "get",
    params: query
  });
}

export function getSelected(query) {
  return request({
    url: "/api/device/deviceSensorType/getSelected",
    method: "get",
    params: query
  });
}

export function changeRsolve(query) {
  return request({
    url: "/api/datahandler/deviceAbnormal/affirmHandle",
    method: "post",
    data:query
  });
}

export function changeFireRsolve(query) {
  return request({
    url: "/api/datahandler/deviceAbnormal/affirmFire",
    method: "post",
    data:query
  });
}
//lch merging 20181025
export function hydrantPageList(query) {
  return request({
    url: "/api/datahandler/deviceFacilitiesAbnormal/pageList",
    method: "get",
    params: query
  });
}

export function fireHydrantName(query) {
  return request({
    url: "/api/device/deviceHardwareFacilities/getSelected",
    method: "get",
    params: query
  });
}

export function affirmHandle(obj) {
  return request({
    url: "/api/datahandler/deviceFacilitiesAbnormal/affirmHandle",
    method: "post",
    data:obj
  });
}

export function getEquipmentType(query) {
  return request({
    url: "/api/device/deviceSensor/getOutdoorSelected",
    method: "get",
    params: query
  });
}
//九大系统类型列表
export function getChannleList(query) {
  return request({
    url: "/api/datahandler/deviceAbnormal/getChannelList",
    method: "get",
    params: query
  });
}

//消防给水记录列表
export function getsChannleList(query) {
  return request({
    url: "/api/datahandler/deviceFacilitiesAbnormal/pageList",
    method: "get",
    params: query
  });
}
//消防给水处理
export function outerdoorHandler(obj) {
  return request({
    url: "/api/datahandler/deviceFacilitiesAbnormal/affirmHandle",
    method: "post",
    data:obj
  });
}
//消防给水异常记录查询
export function getfireHydrantMP(query) {
  return request({
    url: "/api/datahandler/deviceFacilitiesAbnormal/selectMeasuringPoint",
    method: "get",
    params: query
  });
}

//消防主机异常记录查询

export function getRecordList(query) {
  return request({
    url: '/api/datahandler/deviceFireMainAbnormal/pageList',
    method: 'get',
    params: query
  })
}

//处理
export function fmchangeFireRsolve(obj) {
  return request({
    url: '/api/datahandler/deviceFireMainAbnormal/affirmFire',
    method: 'post',
    data: obj
  })
}
//消防用水报警类型
export function exAlrmtype(query) {
  return request({
    url: '/api/datahandler/deviceFacilitiesAbnormal/selectAlrmType',
    method: 'get',
    params: query
  })
}
