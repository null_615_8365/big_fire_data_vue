import request from '@/utils/request'
// 视频分析页面
export function getDevtree(query) {
    return request({
      url: '/api/device/deviceVideoGroup/deviceTree',
      method: 'get',
      params: query
    })
}
//左侧视频设备接口
export function tablelist(query) {
    return request({
      url: '/api/device/deviceVideoExt/queryAll',
      method: 'get',
      params: query
    })
}
//更改显示状态
export function updateShowStatus(obj) {
    return request({
        url: '/api/device/deviceVideoExt/updateShowStatus',
        method: 'post',
        data: obj
    })
}
