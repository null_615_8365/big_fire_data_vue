/*
 * Copyright (c) 2019.  武汉中科图灵科技有限公司
 * Date :  2019/1/5
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.6.2
 */

import request from '@/utils/request'

export function page(query) {
  return request({
    url: '/api/dict/dictType/page',
    method: 'get',
    params: query
  })
}

export function addTypeObj(obj) {
  return request({
    url: '/api/dict/dictType',
    method: 'post',
    data: obj
  })
}

export function getTypeObj(id) {
  return request({
    url: '/api/dict/dictType/' + id,
    method: 'get'
  })
}

export function delTypeObj(id) {
  return request({
    url: '/api/dict/dictType/' + id,
    method: 'delete'
  })
}

export function putTypeObj(id, obj) {
  return request({
    url: '/api/dict/dictType/' + id,
    method: 'put',
    data: obj
  })
}


export function getTree() {
  return request({
    url: '/api/dict/dictType/tree',
    method: 'get'
  })
}
