/*
 * Copyright (c) 2019.  武汉中科图灵科技有限公司
 * Date :  2019/1/5
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.6.2
 */

const getters = {
  sidebar: state => state.app.sidebar,
  token: state => state.user.token,
  avatar: state => state.user.avatar,
  name: state => state.user.name,
  introduction: state => state.user.introduction,
  status: state => state.user.status,
  roles: state => state.user.roles,
  setting: state => state.user.setting,
  permission_routers: state => state.permission.routers,
  addRouters: state => state.permission.addRouters,
  permissionMenus: state => state.user.permissionMenus,
  menus: state => state.user.menus,
  elements: state => state.user.elements,
  adminid: state => state.user.adminid,
  username: state => state.user.username,
  site: state => state.user.site,
  tenantNo: state => state.user.tenantNo,
  planSetup: state => state.pubconfig.planSetup,
  issuperadmin: state => state.user.issuperadmin
}
export default getters
