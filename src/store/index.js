/*
 * Copyright (c) 2019.  武汉中科图灵科技有限公司
 * Date :  2019/1/5
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.6.2
 */

import Vue from 'vue'
import Vuex from 'vuex'
import app from './modules/app'
import permission from './modules/permission'
import user from './modules/user'
import pubconfig from './modules/pubconfig'
import getters from './getters'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    app,
    permission,
    user,
    pubconfig
  },
  getters
})

export default store
