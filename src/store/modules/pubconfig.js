/*
 * Copyright (c) 2019.  武汉中科图灵科技有限公司
 * Date :  2019/1/5
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.6.2
 */

import {
  getImgAuth
} from '@/api/device/tenetConfig';
const pubconfig={
	state:{
		planSetup:0
	},
	mutations:{
		SET_IMG_AUTH:(state,planSetup)=>{
			state.planSetup=planSetup;
		}
	},
	actions:{
		getImgAuth({ commit }){
			return new Promise((resolve, reject) => {
				getImgAuth().then((res)=>{
					if (res.status==200) {
						commit('SET_IMG_AUTH',res.data);
						resolve();
					}
				})
			})
		}
	}
}
export default pubconfig
